#!/usr/bin/bash
# @nu11secur1ty

echo "Active users"
  w
  echo "-------------------------------------------------------------------------"
echo "SSL"
netstat -tn 2>/dev/null | grep :443 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head
  echo "-------------------------------------------------------------------------"

echo "HTTP"
netstat -tn 2>/dev/null | grep :80 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head
  echo "-------------------------------------------------------------------------"

echo "DNS"
netstat -tn 2>/dev/null | grep :53 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head
  echo "-------------------------------------------------------------------------"

echo "MySQL"
netstat -tn 2>/dev/null | grep :3306 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head

  echo "-------------------------------------------------------------------------"
echo "SSH"
netstat -tn 2>/dev/null | grep :22 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head

  echo "-------------------------------------------------------------------------"
echo "FTP"
netstat -tn 2>/dev/null | grep :21 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -nr | head



exit 0;
